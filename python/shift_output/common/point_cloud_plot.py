"""3D Scatter Plot of a cloud of points from a file
or stream with Outsight Serialization format (.osef extension).
"""
# Standard imports
from argparse import ArgumentParser


# Third party imports
import plotly.graph_objects as go
import osef
from osef import osef_frame


def plot_cloud(osef_stream: str) -> None:
    """
    Parse and plot a cloud of points from an osef file.

    :param osef_stream: path or url to the osef file or stream.
    :return: None
    """

    for frame_dict in osef.parse(osef_stream):
        first_frame = frame_dict
        break
    else:
        return

    aug_cloud = osef_frame.AugmentedCloud(first_frame)

    marker_data = go.Scatter3d(
        x=aug_cloud.cartesian_coordinates[0],
        y=aug_cloud.cartesian_coordinates[1],
        z=aug_cloud.cartesian_coordinates[2],
        mode="markers",
        marker=dict(
            size=1.0,
            color=aug_cloud.reflectivities,
            colorscale="Viridis",
            opacity=0.8,
        ),
    )
    layout = go.Layout(scene=dict(aspectmode="data"))  # data: equal aspect ratios
    fig = go.Figure(data=marker_data, layout=layout)
    fig.show()


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="3D Scatter Plot of a cloud of points from a file "
        "or stream with Outsight Serialization format (.osef extension)."
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be plotted (file.osef). "
        "Tcp stream are accepted on the form of tcp://host[:port]. ",
    )
    args = arg_parser.parse_args()

    plot_cloud(args.input)

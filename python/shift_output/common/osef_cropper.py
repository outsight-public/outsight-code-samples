"""Crop sub-OSEF from OSEF input."""
# Standard imports
from argparse import ArgumentParser

# OSEF imports
import osef
from osef.parsing.scanner import count_frames


def crop_osef(file: str, output: str, first: int, last: int) -> None:
    """Crop/Extract frames in range [first, last] of the input OSEF."""
    print("##### OSEF Cropper #####")
    print(
        f"Crop sub-OSEF for frames [{first}, {last}] from {file} ({count_frames(file)} frames)"
    )

    with open(output, "wb") as output_file:
        for frame_index, frame_dict in enumerate(osef.parse(file)):
            if frame_index < first:
                continue
            if last is not None and frame_index > last:
                return
            output_file.write(osef.pack(frame_dict))


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Crop a sub-OSEF file from an OSEF input.")
    arg_parser.add_argument(
        "input",
        metavar="file.osef",
        type=str,
        help="Input file to crop the sub-OSEF from.",
    )
    arg_parser.add_argument(
        "output",
        metavar="file.osef",
        type=str,
        help="Output path for the cropped sub-OSEF.",
    )
    arg_parser.add_argument(
        "--first",
        metavar="N",
        type=int,
        default=0,
        help="Index of the first frame",
    )
    arg_parser.add_argument(
        "--last",
        metavar="M",
        type=int,
        default=None,
        help="Index of the last frame",
    )

    args = arg_parser.parse_args()
    crop_osef(args.input, args.output, args.first, args.last)

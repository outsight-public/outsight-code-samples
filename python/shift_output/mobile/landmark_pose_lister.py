"""Extract data from the OSEF file, output a CSV file and display all landmarks on a map"""
# Standard imports
import argparse
import csv
from typing import Any, Dict, List

# Third party imports
import pandas as pd
import plotly.express as px
import osef
from osef.spec.osef_types import ClassId
from osef import osef_frame

CLASS_NAME = "class_name"
HEIGHT = "height"
LATITUDE = "latitude"
LONGITUDE = "longitude"
OBJECT_ID = "object_id"
TIMESTAMP = "timestamp"


def make_landmark_csv(osef_stream: str, file_path: str) -> None:
    """
    Write in a csv file the geographic latitude and longitude of all landmarks objects
    and sort them by height

    :param osef_stream: path to the osef file to TCP stream if path has form *tcp://hostname:port*
    :param file_path: path to the csv file
    """
    csv_rows: List[Dict[str, Any]] = []

    # extract object data from OSEF stream
    for frame in osef.parse(osef_stream, auto_reconnect=False):
        tracked_objects = osef_frame.TrackedObjects(frame)
        # extract only landmark
        # only save one frame of a specific landmark as its position does not change
        for object_index in range(tracked_objects.number_of_objects):
            if (tracked_objects.class_ids[object_index] == ClassId.LANDMARK) and (
                not any(
                    csv_row[OBJECT_ID] == tracked_objects.object_ids[object_index]
                    for csv_row in csv_rows
                )
            ):
                row = {
                    TIMESTAMP: tracked_objects.timestamp,
                    CLASS_NAME: tracked_objects.class_names[object_index],
                    OBJECT_ID: tracked_objects.object_ids[object_index],
                    HEIGHT: tracked_objects.bbox_sizes[object_index][2],
                    LATITUDE: tracked_objects.geographic_poses[object_index].latitude,
                    LONGITUDE: tracked_objects.geographic_poses[object_index].longitude,
                }

                csv_rows.append(row)
                print(row)

    # Write in csv file
    with open(file_path, "w", encoding="UTF8", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=csv_rows[0].keys())
        writer.writeheader()
        writer.writerows(csv_rows)


def csv_to_map(file_path: str) -> None:
    """
    Display a map with the information defined in the csv file

    :param file_path: path to the csv file
    """
    # read csv file
    csv_rows = pd.read_csv(file_path)
    fig = px.scatter_mapbox(
        data_frame=csv_rows,
        lon=csv_rows[LONGITUDE],
        lat=csv_rows[LATITUDE],
        hover_name=csv_rows[OBJECT_ID],
        color=csv_rows[HEIGHT],
        color_continuous_scale="hot_r",
        range_color=(0, csv_rows[HEIGHT].max()),
        zoom=18,
    )

    # Display the map
    fig.update_layout(mapbox_style="open-street-map")
    fig.update_traces(marker_size=12, selector=dict(mode="markers"))
    fig.show()


# As a script, will unpack all the files passed as command line arguments
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="Decode osef file in from Shift in moving lidar "
        "application and output CSV file"
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be decoded (example: file.osef). "
        "Tcp stream are accepted on the form of tcp://host:port.",
    )
    arg_parser.add_argument(
        "--output",
        "-o",
        type=str,
        default="./landmark_poses.csv",
        help="Path to the output csv file (default: ./landmark_poses.csv)",
    )

    args = arg_parser.parse_args()

    make_landmark_csv(args.input, args.output)
    csv_to_map(args.output)

"""
Process and check if OSEF SLAM indicator has diverged.
"""

# Standard imports
from argparse import ArgumentParser
from collections import namedtuple
from math import isclose

# Third party imports
from osef import osef_frame
import osef

BColorsNamedTuple = namedtuple(
    "BColorsNamedTuple",
    [
        "HEADER",
        "WARNING",
        "ENDC",
    ],
)

BColors = BColorsNamedTuple(HEADER="\033[95m", WARNING="\033[93m", ENDC="\033[0m")


def check_divergence_indicator(
    osef_path: str,
) -> None:
    """
    Parse input data, and print a message when the divergence indicator change value.

    :param osef_path: osef_path/url to parse and check the divergence indicator for.
    :return: None
    """

    print(BColors.HEADER + f"-> Parsing {osef_path}" + BColors.ENDC)

    for frame_number, frame in enumerate(osef.parse(osef_path)):
        # Access the divergence indicator data.
        divergence_indicator_value = osef_frame.EgoMotion(frame).divergence_indicator

        # The SLAM diverged.
        if isclose(divergence_indicator_value, 1.0):
            print(f"SLAM diverged at frame {frame_number} for log {osef_path}")
            return

    # The SLAM did not diverged.
    print(f"SLAM did not diverged for log {osef_path}")


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description=" Decode .osef file and check if the SLAM diverged.\n"
    )
    arg_parser.add_argument(
        "input",
        metavar="file.osef",
        type=str,
        help="File to be decoded. Tcp stream are accepted on the form of tcp://host[:port]. ",
    )

    args = arg_parser.parse_args()
    check_divergence_indicator(args.input)

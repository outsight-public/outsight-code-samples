"""Code sample to show how to filter points that are part of tracked objects"""
# Standard imports
from argparse import ArgumentParser

# Third party imports
import plotly.graph_objects as go
import osef
from osef import osef_frame


def filter_object_points(input_osef: str) -> None:
    """
    Filter points that are part of objects and plot them

    :param input_osef: path or url to the osef file or stream.
    """
    for frame_dict in osef.parse(input_osef):
        # extract data
        augmented_cloud = osef_frame.AugmentedCloud(frame_dict)
        coordinate_cloud = augmented_cloud.cartesian_coordinates
        object_id_cloud = augmented_cloud.object_ids

        object_points = coordinate_cloud[:, object_id_cloud != 0]
        object_point_ids = object_id_cloud[object_id_cloud != 0]

        # plot results
        marker_data = go.Scatter3d(
            x=object_points[0],
            y=object_points[1],
            z=object_points[2],
            mode="markers",
            marker=dict(
                size=3.0,
                color=object_point_ids,
                colorscale="HSV",
                opacity=0.8,
            ),
        )
        layout = go.Layout(scene=dict(aspectmode="data"))  # data: equal aspect ratios
        fig = go.Figure(data=marker_data, layout=layout)
        fig.show()
        break


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Code sample to show how to filter points that are part of tracked objects."
    )
    parser.add_argument(
        "input",
        type=str,
        help="File path to recorded OSEF file. "
        "TCP stream are also accepted on the form of tcp://host[:port].",
    )
    args = parser.parse_args()
    filter_object_points(
        args.input,
    )

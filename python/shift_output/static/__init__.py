"""Examples using a static LiDAR
"""
from . import bounding_box_plot
from . import object_trajectory_plot
from . import tracked_objects_lister

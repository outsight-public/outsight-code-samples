"""Decode osef file in from Shift in ITS application and output csv of tracked objects"""
# Standard imports
import argparse
from typing import Any, Dict, List
from datetime import datetime
import csv

# Third party imports
import numpy as np
import osef
from osef import osef_frame


def _zones_of_obj(_object_id: int, zones: osef_frame.Zones) -> List[str]:
    result = []
    for association in zones.bindings:
        if association.object_id == _object_id:
            result.append(zones.definitions[association.zone_index].name)
    return result


def list_tracked_object(osef_stream: str, file_path: str) -> None:
    """
    Write in a csv file list of tracked objects in a frame.

    :param osef_stream: path to the osef file to TCP stream if path has form *tcp://hostname:port*
    :param file_path: path to the csv file
    """
    # defined csv if output defined
    if file_path:
        csv_rows: List[Dict[str, Any]] = []

    # extract object data from OSEF stream
    for idx, frame in enumerate(osef.parse(osef_stream, auto_reconnect=False)):
        tracked_objects = osef_frame.TrackedObjects(frame)

        try:
            zones = osef_frame.Zones(frame)
        except ValueError:
            zones = None

        for object_index in range(tracked_objects.number_of_objects):
            object_id = tracked_objects.object_ids[object_index]

            bbox_x, bbox_y, bbox_z = tracked_objects.bbox_sizes[object_index]
            row = {
                "frame_number": idx,
                "timestamp": datetime.utcfromtimestamp(tracked_objects.timestamp),
                "object_id": object_id,
                "class": tracked_objects.class_names[object_index],
                "class_id": tracked_objects.class_ids[object_index],
                "speed_kmh": round(
                    3.6 * np.linalg.norm(tracked_objects.speed_vectors[object_index]),
                    2,
                ),
                "volume_m3": round(bbox_x * bbox_y * bbox_z, 2),
                "zones": "|".join(_zones_of_obj(object_id, zones))
                if zones is not None
                else "",
            }

            # If no output needed : just print data
            if file_path:
                csv_rows.append(row)
            else:
                print(row)

        if file_path and len(csv_rows) > 0:
            # Write in csv file if output defined
            with open(file_path, "w", encoding="UTF8", newline="") as file:
                writer = csv.DictWriter(file, fieldnames=csv_rows[0].keys())
                writer.writeheader()
                writer.writerows(csv_rows)


# As a script, will unpack all the files passed as command line arguments
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="Decode osef file in from Shift "
        "in ITS application and output csv of detected objects"
    )
    arg_parser.add_argument(
        "input",
        metavar="file.osef",
        type=str,
        help="File to be decoded. Tcp stream are "
        "accepted on the form of tcp://host:port.",
    )
    arg_parser.add_argument(
        "output",
        metavar="output.csv",
        type=str,
        nargs="?",
        help="Path to csv file to save the output. " "Printed to stdout if omitted.",
    )
    args = arg_parser.parse_args()

    list_tracked_object(args.input, args.output)

# Code Samples - Python

## Scope

This folder contains a python script which is able to read and parse a live 
or recorded stream from Shift.

It also includes code samples which controls Shift through its REST API.

## Files

```
python/
│
├── README.md   # This file
│
├── setup.py    # Installation script 
│
├── requirements.txt    # File containing all example dependancies  
│
├── shift_output/    # Code samples which use osef.parse to extract specific information
│    │
│    ├── mobile/          # Code samples using a mobile LiDAR          
│    │   │
│    │   ├── lidar_poses_lister.py   # Code sample that detectes poses (position and orientation)     
│    │   │                           # of the liDAR        
│    │   │
│    │   ├── super_resolution.py     # Code sample that builds a cloud using multiple frames.
│    │   │                           # We use the pose of the liDAR provided by Shift
│    │   │                           # to build the cloud in the absolute referential.
│    │   │
│    │   ├── landmark_pose_lister.py     # Code sample that plots landmarks objects on a map
│    │   │                               # using gps data
│    │   │
│    │   │
│    │   └── slam_divergence_indicator.py # Code sample parsing an osef log to check if the SLAM diverged
│    │
│    ├── static/          # Code samples using a static LiDAR       
│    │   │        
│    │   ├── bounding_box_plot.py    # 3D Plot of tracked object bounding boxes from a file
│    │   │                           # or stream with Outsight Serialization format (.osef extension).
│    │   │ 
│    │   ├── geo_referenced_object_tracker.py    # Code sample that plots traked objects on a map
│    │   │                                       # using GPS data
│    │   │                           
│    │   │
│    │   ├── object_points.py           # Code sample that filters and plots tracked object points
│    │   │                              
│    │   ├── object_reflectivity.py     # Code sample that filters and plots point reflectivities of tracked objects
│    │   │
│    │   ├── object_trajectory_plot.py  # Code sample that plots trajectories of tracked objects
│    │   │                              # available in an OSEF stream.
│    │   │
│    │   ├── object_super_resolution.py # Code sample that builds a super resolution point cloud 
│    │   │                              # of an object (using multiple scan frames).
│    │   │
│    │   └── tracked_objects_lister.py  # Similar to lidar_poses_lister.py but list tracked objects
│    │                                  # and their properties
│    │
│    └── common/         # Code samples common to static and mobile setup
│        │   
│        ├── osef_printer.py    # Tool to print the OSEF file or stream content
│        │   
│        ├── osef_streamer.py    # Stream an OSEF file on a TCP socket
│        │
│        └── point_cloud_plot.py    # Code sample which uses osef.parse to extract point cloud from
│                                   # stream and plot the first scan in 3d
│
└── shift_configuration/    # Code samples which uses the Shift REST API to configure Shift programmatically
    │ 
    └── process_configuration.py    # Code sample to show how to use Shift REST API to
                                    # upload files, configure and start process 
```

## Installation

The scripts need python3 with some additional modules.

We recommend setting up a python virtual environment to avoid dependency conflicts. 

The modules needed are listed in [setup.py](./setup.py). You can install the package using `pip` in this folder.
``` sh
pip3 install .
```

Examples use the Outsight Python library **[osef](https://pypi.org/project/osef/)** 
which is published on PyPi (Python Package Index). 
This library will be installed when running the `pip install .` command.

See examples to see how to use the **osef** library and its parsing functions.

## Resources
We provide record files in the `resources/` folder that you can use to run samples. 

See this [README](../resources/README.md) to download them. 

## How to run samples

### Live TCP or Records
Record OSEF streamed by Shift via the web interface. 

Then parse the recorded stream:
```sh
python3 ./shift_output/common/osef_printer.py --print-data  ../resources/shift_tracking_mode_record.osef
```

Or directly parse the live stream from Shift by using a TCP url:
```sh
python3 ./shift_output/common/osef_printer.py --print-data tcp://<SHIFT_IP>
```

### Running samples
Script arguments can be found by using the `--help` option (or `-h`).
```sh
python3 ./shift_output/mobile/super_resolution.py --help
```

Here are the command lines to run different examples:

*Static LiDAR:*
```sh
python3 ./shift_output/static/bounding_box_plot.py ../resources/shift_tracking_mode_record.osef
python3 ./shift_output/static/geo_referenced_object_tracker.py ../resources/dusseldorf-static.osef
python3 ./shift_output/static/object_points.py  ../resources/one_frame_tracking_record.osef
python3 ./shift_output/static/object_reflectivity.py  ../resources/one_frame_tracking_record.osef
python3 ./shift_output/static/object_trajectory_plot.py  ../resources/shift_tracking_mode_record.osef
python3 ./shift_output/static/object_super_resolution.py  ../resources/object_super_resolution.osef
python3 ./shift_output/static/tracked_objects_lister.py  ../resources/shift_tracking_mode_record.osef

# Shift configuration
python3 ./shift_configuration/process_configuration.py \
    ../resources/tracking_process_configuration.json \
    --shift-ip=192.168.2.2 \
    --pcap-path=../resources/tracking_record.pcap \
    --zones-path=../resources/tracking_zones.json \
    --lidar-config-path=../resources/ouster_lidar_config.json
```
*Mobile LiDAR*
```sh
python3 ./shift_output/mobile/lidar_poses_lister.py  ../resources/shift_slam_mode_record.osef
python3 ./shift_output/mobile/super_resolution.py ../resources/shift_slam_mode_record.osef --viz --scans 20
python3 ./shift_output/mobile/landmark_pose_lister.py ../resources/shift_mobile_gps_record.osef 
python3 ./shift_output/mobile/slam_divergence_indicator.py ../resources/shift_divergent_relocation.osef

# Shift configuration
python3 ./shift_configuration/process_configuration.py \
    ../resources/mobile_process_configuration.json \
    --shift-ip=192.168.2.2 \
    --pcap-path=../resources/mobile_record.pcap \
    --map-path=../resources/mobile_reference_map.ply
```

*Mobile or Static:*
```sh
python3 ./shift_output/common/osef_printer.py --print-data ../resources/shift_tracking_mode_record.osef
python3 ./shift_output/common/point_cloud_plot.py ../resources/shift_passthrough_mode_record.osef 
python3 ./shift_output/common/osef_streamer.py ../resources/shift_passthrough_mode_record.osef 127.0.0.1
```




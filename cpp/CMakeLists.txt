cmake_minimum_required(VERSION 3.11)

project(ShiftCppExamples)

# Check c++14
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++14" COMPILER_SUPPORTS_CXX14)
if(COMPILER_SUPPORTS_CXX14)
    set(CMAKE_CXX_FLAGS "-std=c++14")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++14 support. Please use a different C++ compiler.")
endif()

# Add external library subdirectory
add_subdirectory(external)

# Add ShiftCpp library subdirectory
add_subdirectory(src)

# Add Apps subdirectories
add_subdirectory(apps/mobile)
add_subdirectory(apps/static)
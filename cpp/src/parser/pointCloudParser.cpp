// Osef headers
#include "osefTypes.h"
#include "typeCaster.h"

// Local headers
#include "parser/pointCloudParser.h"

PointCloudParser::PointCloudParser(const std::string &inputPath, const std::string &outputPath, int maxScans,
				   bool autoReconnect)
	: OsefParser(inputPath, autoReconnect), max_scans(maxScans), ply_writer(outputPath), scan_counter(0)
{
}

int PointCloudParser::parse()
{
	// Need to override this function to handle when max scans have been read.
	int ret = OsefParser::parse();

	if (scan_counter >= max_scans) {
		return 0;
	}

	return ret;
}

int PointCloudParser::parseFrame(const Tlv::tlv_s *frame)
{
	printf("\r[PointCloudParser] Parsing frame %d/%d", scan_counter + 1, max_scans);

	if (!checkValidParsing(frame)) {
		return -1;
	}

	Tlv::Parser parser(frame->getValue(), frame->getLength());
	Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);
	if (!frameTlv) {
		printf("[PointCloudParser] Error: Scan frame not found\n");
		return -1;
	}

	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *augmented_point_cloud = frameParser.findTlv(OSEF_TYPE_AUGMENTED_CLOUD);
	if (!augmented_point_cloud) {
		printf("[PointCloudParser] Error: Augmented point cloud not found\n");
		return -1;
	}

	// To compute the point Cloud, get the cartesian coordinates and apply the pose transformation.
	const Tlv::Parser cloudParser(augmented_point_cloud->getValue(), augmented_point_cloud->getLength());
	const Tlv::tlv_s *cartesian_tlv = cloudParser.findTlv(OSEF_TYPE_CARTESIAN_COORDINATES);
	const Tlv::tlv_s *reflectivities_tlv = cloudParser.findTlv(OSEF_TYPE_REFLECTIVITIES);
	const Tlv::tlv_s *pose_tlv = frameParser.findTlv(OSEF_TYPE_POSE);
	const Tlv::tlv_s *number_points = cloudParser.findTlv(OSEF_TYPE_NUMBER_OF_POINTS);

	if (!cartesian_tlv || !pose_tlv || !number_points || !reflectivities_tlv) {
		printf("[PointCloudParser] Error: Cartesion and/or Pose and/or points  and/or reflectivities not found\n");
		return -1;
	}

	const uint32_t points = NumberOfObjects::castValue(number_points);
	const Eigen::Vector3f pose_translation(Pose::castTranslationValue(pose_tlv).data());
	const Eigen::Matrix3f pose_rotation(Pose::castRotationValue(pose_tlv).data());

	Eigen::Map<Eigen::MatrixXf> pointsMatrix((float *)(cartesian_tlv->getValue()), 3, points);
	std::vector<int> reflec(reflectivities_tlv->getValue(), reflectivities_tlv->getValue() + points);
	Eigen::Map<Eigen::VectorXi> reflectivities(reflec.data(), points);

	pointsMatrix.matrix() = pose_rotation * pointsMatrix;
	pointsMatrix.colwise() += pose_translation;

	ply_writer.writePoints(pointsMatrix, reflectivities);

	scan_counter++;
	if (scan_counter >= max_scans) {
		printf("\n[PointCloudParser] Max scans (%d) have been read, parsing stopped.\n", max_scans);
		return -1;
	}

	return 0;
}

### CMakeFile to compile all the mobile examples.

# SuperResolution application
add_executable(superResolution superResolution.cpp)
target_link_libraries(superResolution PRIVATE shift_cpp)
target_compile_options(superResolution PRIVATE -Wall -Wextra)

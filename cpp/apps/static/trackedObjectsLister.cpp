// Local headers
#include "parser/trackedObjectsParser.h"

int main(int argc, char **argv)
{
	// If called with no argument
	if (argc < 3) {
		printf("[TrackedObjectsLister] Usage :\n");
		printf("\t%s /path/to/recordedStream.osef path/to/output.csv\n", argv[0]);
		printf("\t%s tcp://192.168.1.2 path/to/output.csv\n", argv[0]);
		printf("\t%s tcp://192.168.1.2:11120 path/to/output.csv\n", argv[0]);
		return -1;
	}

	TrackedObjectsParser parser(argv[1], argv[2]);
	return parser.parse();
}
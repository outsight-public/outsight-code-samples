# Code Samples - C++

## Scope

This folder contains source code to build an example app which is able to read and parse a live or recorded stream from an Outsight Shift.

It is not meant to be used as a library but just as example code which can be entirely or partially copied in a project.
This way one can quickly build a C++ application to exploit the data from Shift, or add Shift data input to an existing application.

It does not include (yet) any code to use the control Shift API, only code to exploit the output data.

## Files

```
cpp
├── README.md                           # This file
├── CMakeLists.txt                      # To build Shift Cpp project
├── apps
    ├── mobile
        ├── superResolution.cpp         # Code sample that builds a cloud using multiple frames.
    ├── static
        ├── trackedObjectLister.cpp     # List tracked objects and their properties. 
├── external
    ├── osef                            # Library to parse an osef file/stream
├── include       
    ├── parser
        ├── pointCloudParser.h          # ???
        ├── trackedObjectsParser.h      # ???
    ├── writer
         ├── plyWriter.h                     # Writer of PLY file (PointCloud)
└── src
    ├── plyWriter.cpp                   # Implementation of PLY Writer
```


## Resources
We provide record files in the `resources/` folder that you can use to run samples. 

See this [README](../resources/README.md) to download them. 

## Building and running example application

The example application can be built from source using cmake build system.

### Linux

On a Linux system with compilation tools available, the following commands must do the job:
``` sh
cd cpp # if needed
cmake .
make
```

### Windows

On Windows, you can follow Microsoft instructions to import the project in visual studio: 
[CMake projects in Visual Studio](https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio).

## Running example applications

### Mobile

#### 1. SuperResolution

Parse an OSEF stream (file or live) and generate the point cloud file (PLY) with a defined amount of frames (30 by default):

``` sh
./apps/mobile/superResolution ../resources/shift_slam_mode_record.osef result/cloud.ply
```
``` sh
./apps/mobile/superResolution tcp://192.168.2.2 result/cloud.ply
```
``` sh
./apps/mobile/superResolution ../resources/shift_slam_mode_record.osef result/cloud.ply 20
```

### Static

#### 1. TrackedObjectLister

List tracked objects and their properties from an OSEF stream to an output CSV file:

``` sh
./apps/static/trackedObjectsLister ../resources/shift_tracking_mode_record.osef result/output.csv
```
